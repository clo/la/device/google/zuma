#
# Copyright (C) 2024 The Android Open-Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

ifeq ($(TARGET_BOOTS_16K),true)
# Configures the 16kb kernel directory.
TARGET_KERNEL_DIR := $(TARGET_KERNEL_DIR)/16kb

else ifeq ($(PRODUCT_16K_DEVELOPER_OPTION),true)
# Configures the 16kb kernel and modules for OTA updates.
TARGET_KERNEL_DIR_16K := $(TARGET_KERNEL_DIR)/16kb
BOARD_KERNEL_PATH_16K := $(TARGET_KERNEL_DIR_16K)/Image.lz4
BOARD_PREBUILT_DTBOIMAGE_16KB := $(TARGET_KERNEL_DIR_16K)/dtbo.img
BOARD_16K_OTA_USE_INCREMENTAL := true
BOARD_16K_OTA_MOVE_VENDOR := true

# BOARD_KERNEL_MODULES_LOAD_16K contains the module names that
# will be loaded in the First Boot Stage.
#
# For example:
#     <module-name-1>.ko <module-name-2>.ko
#
# The modules will be extracted from these kernel files:
#
#     vendor_kernel_boot.modules.load
#     system_dlkm.modules.load
#     vendor_dlkm.modules.load
#
# and we will use the kernel blocklist files below to remove the
# modules that shouldn't be loaded in First Stage Boot.
#
#     system_dlkm.modules.blocklist
#     vendor_dlkm.modules.blocklist
BOARD_KERNEL_MODULES_LOAD_16K := $(file < $(TARGET_KERNEL_DIR_16K)/vendor_kernel_boot.modules.load)
BOARD_KERNEL_MODULES_LOAD_16K += $(file < $(TARGET_KERNEL_DIR_16K)/system_dlkm.modules.load)
BOARD_KERNEL_MODULES_LOAD_16K += $(file < $(TARGET_KERNEL_DIR_16K)/vendor_dlkm.modules.load)
BOARD_KERNEL_MODULES_LOAD_16K := $(foreach module,$(BOARD_KERNEL_MODULES_LOAD_16K),$(notdir $(module)))

# Extract all the blocklist modules from (system_dlkm|vendor_dlkm).modules.blocklist
# The blocklist file has this format:
#
#     blocklist module-name.ko
#     blocklist alias_module_name
BOARD_KERNEL_MODULES_BLOCKLIST_16K := $(filter-out blocklist,$(file < $(TARGET_KERNEL_DIR_16K)/system_dlkm.modules.blocklist))
BOARD_KERNEL_MODULES_BLOCKLIST_16K += $(filter-out blocklist,$(file < $(TARGET_KERNEL_DIR_16K)/vendor_dlkm.modules.blocklist))

# Remove the .ko suffix from the blocklist modules. The aliases for the modules don't have
# the .ko suffix.
BOARD_KERNEL_MODULES_BLOCKLIST_16K := $(BOARD_KERNEL_MODULES_BLOCKLIST_16K:%.ko=%)

# The aliases for the modules might use underscore ( _ ), replace the underscore for dash ( - ) and
# concatenate it. This will also apply for the module names.
BOARD_KERNEL_MODULES_BLOCKLIST_16K += $(subst _,-,$(BOARD_KERNEL_MODULES_BLOCKLIST_16K))

# Add the .ko suffix to all the blocklist modules.
BOARD_KERNEL_MODULES_BLOCKLIST_16K := $(BOARD_KERNEL_MODULES_BLOCKLIST_16K:%=%.ko)

# Remove all the blocklisted modules from BOARD_KERNEL_MODULES_LOAD_16K.
BOARD_KERNEL_MODULES_LOAD_16K := $(filter-out $(BOARD_KERNEL_MODULES_BLOCKLIST_16K),$(BOARD_KERNEL_MODULES_LOAD_16K))

# Remove the bcm_dbg.ko module. This is a special module that is not loaded by default,
# although it appears in the vendor_dlkm.modules.load but not in the vendor_dlkm.modules.blocklist.
# See b/380780525
BOARD_KERNEL_MODULES_LOAD_16K := $(filter-out bcm_dbg.ko,$(BOARD_KERNEL_MODULES_LOAD_16K))

# BOARD_KERNEL_MODULES_16K contains the full path of modules that
# will be copied to the ramdisk and loaded in the First Boot Stage.
#
# For example:
#     device/google/<device>-kernels/<kernel-version>/<kernel-release>/16kb/<module-name>.ko
BOARD_KERNEL_MODULES_16K := $(foreach module,$(BOARD_KERNEL_MODULES_LOAD_16K),$(TARGET_KERNEL_DIR_16K)/$(notdir $(module)))

# Extract the modules to load in the Second Boot Stage from init.insmod.<device>.cfg file.
BOARD_VENDOR_KERNEL_MODULES_2ND_STAGE_16KB_MODE := $(file < $(TARGET_KERNEL_DIR_16K)/init.insmod.$(PRODUCT_DEVICE).cfg)
BOARD_VENDOR_KERNEL_MODULES_2ND_STAGE_16KB_MODE := $(subst modprobe|,,$(BOARD_VENDOR_KERNEL_MODULES_2ND_STAGE_16KB_MODE))
BOARD_VENDOR_KERNEL_MODULES_2ND_STAGE_16KB_MODE := $(filter %.ko,$(BOARD_VENDOR_KERNEL_MODULES_2ND_STAGE_16KB_MODE))

endif
